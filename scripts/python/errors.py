#!/usr/bin/env python3
"""
Module with different errors for a progeny calculator.
"""


class InputError(Exception):
    """
    Parent class for simulating input errors.
    """
    pass


class InvalidAllelesError(InputError):
    """
    Simulates an error where the allele is incorrect
    """
    pass


class InvalidCharacterError(InputError):
    """
    Simulates an error where the genotype contains wrong characters.
    """
    pass


class GenotypeLengthError(InputError):
    """
    Simulates an error where the two parent genotypes aren't the same length.
    """
    pass
