#!/usr/bin/env python3
"""
This program gets the genotypes of two parents and calculates the chances of the appearance of the
offspring, would these two parents mate.

Usage: ./web_calculator.py
"""

# IMPORTS
import cgi
import progeny_calculator
import commandline_calculator
import person
import sys


# METADATA
__author__ = "Wendy van der Meulen & Niels van der Vegt"
__version__ = "0.1"


# CLASSES
class WebCalculator(progeny_calculator.ProgenyCalculator):
    def __init__(self):
        self.web_args = {"mother": "AA", "father": "aa", "trait_1": "Haarkleur", "dominance_1": "Blond", "recessive_1": "Bruin"}
        self.father = person.Parent(self.web_args["father"])
        self.mother = person.Parent(self.web_args["mother"])
        self.child = person.Child(self.father, self.mother)
        self.check_input()
        self.print_html_page()

    def check_input(self):
        """
        Checks if the given input is valid.
        """
        if int((len(self.web_args) - 2) / 3) != len(set(self.mother.genotype.upper())):
            self.print_html_page("incomplete")
        # for trait in self.web_args[1::4]:
        #     print(self.web_args[1::3])
        #     if not re.fullmatch("[a-zA-Z0-9]+", trait):
        #         self.print_html_page("invalid")

    def get_labels_fenotypes(self):
        labels_dict = {}
        for fenotype in self.child.possible_fenotypes:
            label = []
            for i in range(int((len(self.web_args) - 2) / 3)):
                trait = "trait_" + str(i + 1)
                dominance = "dominance_" + str(i + 1)
                recessive = "recessive_" + str(i + 1)
                if fenotype[i].islower():
                    if not self.web_args[recessive].endswith("e"):
                        label.append(" ".join(["".join([self.web_args[recessive], "e"]),
                                               self.web_args[trait]]))
                    else:
                        label.append(" ".join([self.web_args[recessive], self.web_args[trait]]))
                else:
                    if not self.web_args[recessive].endswith("e"):
                        label.append(" ".join(["".join([self.web_args[dominance], "e"]),
                                               self.web_args[trait]]))
                    else:
                        label.append(" ".join([self.web_args[dominance], self.web_args[trait]]))
            labels_dict[fenotype] = ", ".join(label)
        return labels_dict

    def fuse_labels_and_percentages(self):
        """
        """
        fenotype_information_dict = {}
        fenotype_labels = self.get_labels_fenotypes()
        for key_label, value_label in zip(fenotype_labels.keys(), fenotype_labels.values()):
            for key_per, value_per in zip(self.child.percentage_fenotype.keys(), self.child.percentage_fenotype.values()):
                if key_label == key_per:
                    fenotype_information_dict[key_label] = [value_label, value_per]

        return fenotype_information_dict

    def write_piechart_scripts(self):
        """
        """
        f_script = "../javascript/pie_f.js"
        g_script = "../javascript/pie_g.js"
        for script in [f_script, g_script]:
            with open(script, "w") as open_script:
                open_script.write('''
                google.charts.load('current', {'packages':['corechart']});
                google.charts.setOnLoadCallback(drawChart);

                function drawChart() {

                var data = new google.visualization.DataTable();
                data.addColumn('string', 'label');
                data.addColumn('number', 'percentage');
                data.addRows([''')
                if "f" in script:
                    for ls in self.fuse_labels_and_percentages().values():
                        open_script.write("[")
                        open_script.write('''"''' + ls[0] + '''"''' + ", ")
                        open_script.write(str(float(ls[1])))
                        open_script.write("]")
                        open_script.write(",\n")
                else:
                    for genotype, perc in zip(self.child.percentage_genotype.keys(), self.child.percentage_genotype.values()):
                        open_script.write("[")
                        open_script.write('''"''' + genotype + '''"''' + ", ")
                        open_script.write(str(float(perc)))
                        open_script.write("]")
                        open_script.write(",\n")
                open_script.write(''']);

                      var options = {
                        title: 'Mogelijke ''')
                if "f" in script:
                    open_script.write("fenotypen")
                else:
                    open_script.write("genotypen")

                open_script.write(''' voor een kind',
                        sliceVisibilityThreshold: .05
                      };

                      var chart = new google.visualization.PieChart(document.getElementById(''')
                if "f" in script:
                    open_script.write("'piechart_f'")
                else:
                    open_script.write("'piechart_g'")
                open_script.write(''');
                      chart.draw(data, options);
                    }
                ''')
        return 0

    def print_html_page(self, way="0"):
        print('Content-type: text/html\n\n')
        print('''
                <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
                "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
                <html xmlns="http://www.w3.org/1999/xhtml">
                  <head>
                    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
                    <script type="text/javascript" src="../javascript/pie_f.js"></script>
                    <script type="text/javascript" src="../javascript/pie_g.js"></script>
                    <link rel="stylesheet" type="text/css" href="../../css/style.css"/>
                    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
                    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css"/>
                    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                    <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
                    <script src="../javascript/script.js" type="text/javascript"></script>
                    <title>Bio-calculator</title>
                  </head>
                  <body>
                    <div class="topcontainer">
                      <h1><a href="index.html">BioGen</a></h1>
                      <h3><i>Alles over biologie en genetica</i></h3>
                      <div id="cssmenu">
                        <ul>
                              <li>
                                  <a href="../../index.html">Home</a>
                              </li>
                              <li class ="active">
                                  <a href="../../bio_calculator.html">Bio-calculator</a>
                              </li>
                              <li>
                                  <a href="../../werking.html">Werking</a>
                              </li>
                              <li>
                                  <a href="../../proces.html">Proces</a>
                              </li>
                              <li class="last">
                                  <a href="../../mendel.html">Mendel</a>
                              </li>
                                </ul>
                      </div>
                      <div id="dropdownmenu">
                        <ul>
                          <li class="has-sub">
                              <a href="#">
                                  <img src="../../images/drop_menu.png" alt=""/>
                                  </a>
                            <ul>
                                  <li>
                                    <a href="../../index.html">Home</a>
                                  </li>
                                  <li class ="active">
                                    <a href="../../bio_calculator.html">Bio-calculator</a>
                                  </li>
                                  <li>
                                      <a href="../../werking.html">Werking</a>
                                  </li>
                                  <li>
                                      <a href="../../proces.html">Proces</a>
                                  </li>
                                  <li>
                                      <a href="../../mendel.html">Mendel</a>
                                  </li>
                            </ul>
                          </li>
                        </ul>
                      </div>
                    </div>
                    <div class="container">
                      <p>Bereken hier wat de mogelijke genotypen
                      en fenotypen kunnen zijn voor een kind.</p>
            ''')
        if way == "incomplete":
            print('''
                      <p class="red">Vul alsjeblieft alles in.</p>
                      <form id="form" action="web_calculator.cgi" method="post">
                        <div>
                          <fieldset class="invisible">
                            <legend>Parents:</legend>
                              <label>Genotype moeder:<br/>
                              <input type="text" name="mother" class="invulform"
                              value="''' + self.mother.genotype + '''"/></label><br/>
                              <label>Genotype vader:<br/>
                              <input type="text" name="father" class="invulform"
                              value="''' + self.father.genotype + '''"/></label><br/>
                          </fieldset>
                ''')
            for count, trait in enumerate(person.Parent.get_alleles(self.mother)):
                print('''
                          <fieldset>
                            <legend>Eigenschap ''' + trait[0].upper() + ''':</legend>
                              <label>Waar staat de eigenschap''', trait[0].upper(), '''in het algemeen voor?<br/>
                              <input type="text" name="trait_''' + str(count + 1) + '''" class="invulform"/></label><br/>
                              <label>Waar staat het dominante allel voor?<br/>
                              <input type="text" name="dominance_''' + str(count + 1) + '''" class="invulform"/></label><br/>
                              <label>Waar staat het recessieve allel voor?<br/>
                              <input type="text" name="recessive_''' + str(count + 1) + '''" class="invulform"/></label><br/>
                          </fieldset>''')

            print('''
                          <input id="submit" type="submit" value="Submit" name="submit"/>
                        </div>
                      </form>''')
        else:
            mother = self.mother.genotype
            traits = self.web_args
            for i in range(int((len(traits) - 2) / 3)):
                trait = "trait_" + str(i + 1)
                dominance = "dominance_" + str(i + 1)
                recessive = "recessive_" + str(i + 1)
                print('''
                    <div class="traits">
                      <p>Eigenschap ''' + sorted(list(set(mother.upper())))[i] + ''' is ''' + traits[trait] + ''':<br/>
                        Dominant is ''' + traits[dominance] + '''.<br/>
                        Ressesief is ''' + traits[recessive] + '''.<br/>
                      </p>
                    </div>
                    ''')
            print('''
                    <div>
                      <p>Onderaan staat een piechart van de fenotype en genotype data.</p>''')
            self.print_results(self.father, self.mother, self.child, True)
            self.write_piechart_scripts()
            print('''
                <div id="piechart_f"></div><div id="piechart_g></div>
            ''')
        print('''
                </div>
              </body>
            </html>
            ''')
        sys.exit()


# MAIN
def main():
    # if len(cgi.FieldStorage()) == 0:
    #     commandline_calculator.CommandlineCalculator()
    # else:
    WebCalculator()

    return 0


# ENTRY POINT
if __name__ == "__main__":
    sys.exit(main())
