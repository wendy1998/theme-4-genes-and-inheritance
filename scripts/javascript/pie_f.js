
                google.charts.load('current', {'packages':['corechart']});
                google.charts.setOnLoadCallback(drawChart);

                function drawChart() {

                var data = new google.visualization.DataTable();
                data.addColumn('string', 'label');
                data.addColumn('number', 'percentage');
                data.addRows([["AAe AA, AAe AA", 50.0],
["AAe AA, AAe AA", 50.0],
]);

                      var options = {
                        title: 'Mogelijke fenotypen voor een kind',
                        sliceVisibilityThreshold: .01
                      };

                      var chart = new google.visualization.PieChart(document.getElementById('piechart_f'));
                      chart.draw(data, options);
                    }
                