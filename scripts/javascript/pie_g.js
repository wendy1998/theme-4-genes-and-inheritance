
                google.charts.load('current', {'packages':['corechart']});
                google.charts.setOnLoadCallback(drawChart);

                function drawChart() {

                var data = new google.visualization.DataTable();
                data.addColumn('string', 'label');
                data.addColumn('number', 'percentage');
                data.addRows([["aaBb", 50.0],
["aabb", 50.0],
]);

                      var options = {
                        title: 'Mogelijke genotypen voor een kind',
                        sliceVisibilityThreshold: .01
                      };

                      var chart = new google.visualization.PieChart(document.getElementById('piechart_g'));
                      chart.draw(data, options);
                    }
                